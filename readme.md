### Setup ###

- Projekt auschecken, keystore ablegen und ggf. unter tomcat.properties umstellen.
- Starten der Applikation durch ausführen der Application.java
- Über Console: java -jar book-1.0-SNAPSHOT.jar


### Run Tomcat Embedded ###

1. mvn clean install
2. Run with "java -jar book-1.0-SNAPSHOT.jar" 

### Run external Tomcat ###

1. mvn clean package -Pwar
2. Deploy war file to tomcat

### Run Postgres HEROKU ###

default

### Run H2 Embedded Local ###
-Dspring.config.location=classpath:/application.properties,classpath:/h2.properties

### Services ###

Dummy Login:
GET http://localhost:8080/login?username=user&password=password

Folgende Services sind authorisiert ( X-AUTH-TOKEN = Token -> muss als HTTP Header gesetzt werden ):

GET http://localhost:8080/books{/id}
POST http://localhost:8080/books 
GET http://localhost:8080/authors{/id}


### Metrics ###

GET http://localhost:8080/health
GET http://localhost:8080/metrics
GET http://localhost:8080/trace
GET http://localhost:8080/mappings
