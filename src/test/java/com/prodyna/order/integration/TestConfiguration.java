package com.prodyna.order.integration;

import static java.util.Arrays.asList;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class TestConfiguration {

    @Bean
    public String url() {
	    return "http://127.0.0.1:9099";
    }
    
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setMessageConverters(messageConverter());
		return restTemplate;
	}
	
	@Bean
	public ObjectMapper objectMapper() {
	    return new ObjectMapper();
	}

	@Bean
	public List<HttpMessageConverter<?>> messageConverter() {
		return asList(new FormHttpMessageConverter(), new StringHttpMessageConverter(), new MappingJackson2HttpMessageConverter());
	}
}
