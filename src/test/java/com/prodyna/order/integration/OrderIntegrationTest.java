package com.prodyna.order.integration;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.prodyna.order.dto.Order;
import com.prodyna.order.dto.OrderItem;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class OrderIntegrationTest {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private String url;
	
	@Test
	public void testSaveOrder() {
		Order order = new Order();
		OrderItem item = new OrderItem();
		item.setCount(2);
		item.setItemId("http://localhost:8080/book-service/books/1");
		order.setOrderItems(Arrays.asList(item));
		ResponseEntity<Order> exchange = restTemplate.exchange(url + "/order", HttpMethod.POST, new HttpEntity<Order>(order), Order.class);
		Assert.assertNotNull(exchange.getBody());
		System.out.println(exchange.getBody());
	}
}
