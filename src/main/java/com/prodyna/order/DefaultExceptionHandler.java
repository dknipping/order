package com.prodyna.order;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.common.collect.Maps;
import com.prodyna.order.service.exception.ServiceException;

@ControllerAdvice
public class DefaultExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @RequestMapping
    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody Map<String, Object> handleServiceExcpetion(ServiceException ex) throws IOException {
        LOG.error("An ServiceException occurs", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "ServiceException");
        map.put("cause", ex.getMessage());
        return map;
    }
    
    @RequestMapping
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody Map<String, Object> handleUncaughtException(Exception ex) throws IOException {
        LOG.error("Unhandled Exception", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Unknown Error");
        if (ex.getCause() != null) {
            map.put("cause", ex.getCause().getMessage());
        } else {
            map.put("cause", ex.getMessage());
        }
        return map;
    }
    
    @RequestMapping
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody Map<String, Object> handleValidationtException(MethodArgumentNotValidException ex) throws IOException {
        LOG.error("MethodArgumentNotValidException", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", ex.getBindingResult().getFieldError().getCode());
        map.put("field", ex.getBindingResult().getFieldError().getField());
        return map;
    }
    
    
}