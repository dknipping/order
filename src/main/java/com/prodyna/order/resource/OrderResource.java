package com.prodyna.order.resource;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prodyna.order.dto.Order;
import com.prodyna.order.service.OrderService;

@RestController
public class OrderResource {

	@Autowired
	private OrderService service;
	
	@RequestMapping(value = "/order/{id}", method = GET)
    public HttpEntity<Order> getOrder(@RequestParam Long id) {
		return new ResponseEntity<Order>(service.getOrder(id), OK);
    }
	
	@RequestMapping(value = "/order", method = POST)
    public HttpEntity<Order> save(@RequestBody Order order) {
		Order saveOrder = service.saveOrder(order);
		return new ResponseEntity<Order>(saveOrder, OK);
    }
}
