//package com.prodyna.order.security;
//
//import java.io.IOException;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Component;
//
//@Component
//public class AuthenticationTokenFilter implements Filter {
//    public static final String X_AUTH_TOKEN_HEADER = "X-AUTH-TOKEN";
//
//    @Autowired
//    private AuthenticationService authenticationService;
//
//    @Override
//    public void init(FilterConfig fc) throws ServletException {
//
//    }
//
//    @Override
//    public void doFilter(ServletRequest req, ServletResponse res, FilterChain fc) throws IOException, ServletException {
//        String header = ((HttpServletRequest) req).getHeader(X_AUTH_TOKEN_HEADER);
//        if (header != null) {
//            TokenAuthentication tokenAuthentication = (TokenAuthentication) authenticationService.getAuthentication(header);
//            SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
//        }
//        fc.doFilter(req, res);
//    }
//
//    @Override
//    public void destroy() {
//    }
//
//}