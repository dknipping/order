//package com.prodyna.order.security;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.stereotype.Component;
//
//@Component
//public class TokenAuthenticationProvider implements AuthenticationProvider {
//
//    @Autowired
//    private AuthenticationService authenticationService;
//
//    @Override
//    public Authentication authenticate(Authentication auth) throws AuthenticationException {
//        if (!auth.isAuthenticated()) {
//            String token = authenticationService.getToken(auth);
//            if (token != null) {
//                Authentication authentication = authenticationService.getAuthentication(token);
//                if (authentication != null) {
//                	return authentication;
//                } else {
//                	throw new BadCredentialsException("Token not valid " + token);	
//                }
//            } else {
//                throw new BadCredentialsException("Token not valid " + token);
//            }
//        }
//        return auth;
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return authentication.equals(TokenAuthentication.class);
//    }
//}