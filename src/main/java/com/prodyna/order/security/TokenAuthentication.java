//package com.prodyna.order.security;
//
//import java.util.Collection;
//
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//
//public class TokenAuthentication implements Authentication {
//
//    private static final long serialVersionUID = 1L;
//    private String username;
//    private String token;
//    private Collection<? extends GrantedAuthority> authorities;
//
//    public TokenAuthentication(String username, String token, Collection<? extends GrantedAuthority> authorities) {
//        this.username = username;
//        this.token = token;
//        this.authorities = authorities;
//    }
//
//    @Override
//    public String getName() {
//        return username;
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return this.authorities;
//    }
//
//    @Override
//    public Object getCredentials() {
//        return token;
//    }
//
//    @Override
//    public Object getDetails() {
//        return null;
//    }
//
//    @Override
//    public Object getPrincipal() {
//        return null;
//    }
//
//    @Override
//    public boolean isAuthenticated() {
//        return true;
//    }
//
//    @Override
//    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
//        throw new IllegalStateException("Authenticated cannot be set");
//    }
//
//}