//package com.prodyna.order.security;
//
//import static java.util.Arrays.asList;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.UUID;
//
//import org.springframework.context.ApplicationEvent;
//import org.springframework.context.ApplicationEventPublisher;
//import org.springframework.context.ApplicationEventPublisherAware;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
//import org.springframework.stereotype.Service;
//
//@Service
//public class AuthenticationService implements ApplicationEventPublisherAware {
//
//	private Map<String, TokenAuthentication> tokenCache = new HashMap<String, TokenAuthentication>();
//	private ApplicationEventPublisher applicationEventPublisher;
//
//	public TokenAuthentication authenticate(String username, String password) {
//		if ("user".equals(username) && "password".equals(password)) {
//			String token = UUID.randomUUID().toString();
//			TokenAuthentication authentication = new TokenAuthentication(username, token, asList(new SimpleGrantedAuthority("ROLE_USER")));
//			tokenCache.put(token, authentication);
//			return authentication;
//		}
//		publishEvent(new AuthenticationFailureBadCredentialsEvent(new PreAuthenticatedAuthenticationToken(username, password), new BadCredentialsException("Failed to authenticate")));
//		return null;
//	}
//
//	public TokenAuthentication getAuthentication(String token) {
//		if (token != null) {
//			return tokenCache.get(token);
//		}
//		return null;
//	}
//
//	public String getToken(Authentication authentication) {
//		return authentication.getCredentials().toString();
//	}
//	
//	private void publishEvent(ApplicationEvent event) {
//		if (applicationEventPublisher != null) {
//			applicationEventPublisher.publishEvent(event);
//		}
//	}
//
//	@Override
//	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
//		this.applicationEventPublisher = applicationEventPublisher;
//	}
//}
