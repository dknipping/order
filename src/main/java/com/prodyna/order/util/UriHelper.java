package com.prodyna.order.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UriHelper {

	private static final Logger LOG = LoggerFactory.getLogger(UriHelper.class);
	
	public static String getIdFromUrl(String url) {
		URL uri = null;
		try {
			uri = new URL(url);
			return uri.getPath();
		} catch (MalformedURLException e) {
			LOG.error("Url not malformed", e);
		}
		return null;
	}
	
	public static String getServiceName(String url) {
		return url.split("/")[1];
	}
}
