package com.prodyna.order;

import org.springframework.boot.SpringApplication;

public class Application {

	public static void main(String[] args) {
		SpringApplication.run(CoreConfiguration.class, args);
	}
}
