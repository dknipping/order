//package com.prodyna.order.configuration;
//
//import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
//
//import com.prodyna.order.security.AuthenticationTokenFilter;
//import com.prodyna.order.security.TokenAuthenticationProvider;
//
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true)
//@Configuration
//public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    private AuthenticationTokenFilter authenticationTokenFilter;
//    
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.addFilterBefore(authenticationTokenFilter, BasicAuthenticationFilter.class);
//        http.csrf().disable().sessionManagement().sessionCreationPolicy(STATELESS);
//        http.authorizeRequests()
//        		.antMatchers(HttpMethod.OPTIONS, "/**").anonymous()
//                .antMatchers("/login").anonymous()
//                .antMatchers("/**").hasRole("USER")
//            .and().formLogin().disable()
//                  .httpBasic().disable();
//    }
//    
//    
//    @Bean
//    public AuthenticationProvider authenticationProvider() {
//        return new TokenAuthenticationProvider();
//    }
//    
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);
//        auth.authenticationProvider(authenticationProvider());
//    }
//    
//    @Bean
//    @Override
//    protected AuthenticationManager authenticationManager() throws Exception {
//        return super.authenticationManager();
//    }
//}
