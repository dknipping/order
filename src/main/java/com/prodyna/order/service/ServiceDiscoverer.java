package com.prodyna.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.DiscoveryClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class ServiceDiscoverer {
	
	@Autowired
	private DiscoveryClient discoveryClient;
	
	@HystrixCommand(fallbackMethod = "discoveryFailed")
	public String discoverService(String serviceName) {
		InstanceInfo instance = discoveryClient.getNextServerFromEureka(serviceName, false);
		return instance.getHomePageUrl();
	}
	
	public String discoveryFailed(String serviceName) {
		return null;
	}
}