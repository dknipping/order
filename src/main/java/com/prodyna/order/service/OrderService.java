package com.prodyna.order.service;

import static org.apache.commons.lang3.StringUtils.removeStart;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

import com.prodyna.order.dto.Order;
import com.prodyna.order.dto.OrderItem;
import com.prodyna.order.entity.OrderEntity;
import com.prodyna.order.repository.OrderRepository;
import com.prodyna.order.util.UriHelper;

@Service
public class OrderService {

	private OrderRepository orderRepository;
	private ConversionService conversionService;
	private ServiceDiscoverer serviceDiscoverer;

	@Autowired
	public OrderService(OrderRepository orderRepository, ConversionService conversionService, ServiceDiscoverer serviceDiscoverer) {
		this.orderRepository = orderRepository;
		this.conversionService = conversionService;
		this.serviceDiscoverer = serviceDiscoverer;
	}

	public Order saveOrder(Order order) {
		OrderEntity save = orderRepository.save(conversionService.convert(order, OrderEntity.class));
		Order convertedOrder = conversionService.convert(save, Order.class);
		List<OrderItem> orderItems = convertedOrder.getOrderItems();
		for (OrderItem orderItem : orderItems) {
			String serviceName = UriHelper.getServiceName(orderItem.getItemId());
			String discoverService = serviceDiscoverer.discoverService(serviceName);
			if (discoverService != null) {
				orderItem.add(new Link(discoverService + removeStart(orderItem.getItemId(), "/"), "item"));
			}
		}
		return convertedOrder;
	}

	public Order getOrder(Long id) {
		return conversionService.convert(orderRepository.findOne(id), Order.class);
	}
}
