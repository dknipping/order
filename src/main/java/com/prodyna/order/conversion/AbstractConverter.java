package com.prodyna.order.conversion;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import com.netflix.discovery.DiscoveryClient;

public abstract class AbstractConverter<S, T> implements Converter<S, T>, ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    protected ConversionService conversionService() {
        return applicationContext.getBean(ConversionService.class);
    }

    protected DiscoveryClient discoveryClient() {
    	return applicationContext.getBean(DiscoveryClient.class);
    }
}
