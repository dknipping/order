package com.prodyna.order.conversion;

import org.springframework.stereotype.Component;

import com.prodyna.order.dto.OrderItem;
import com.prodyna.order.entity.OrderItemEntity;
import com.prodyna.order.util.UriHelper;

@Component
public class OrderItemToOrderItemEntityConverter extends AbstractConverter<OrderItem, OrderItemEntity> {

    @Override
    public OrderItemEntity convert(OrderItem item) {
    	OrderItemEntity itemEntity = new OrderItemEntity();
    	itemEntity.setCount(item.getCount());
    	itemEntity.setRefId(UriHelper.getIdFromUrl(item.getItemId()));
        return itemEntity;
    }

}
