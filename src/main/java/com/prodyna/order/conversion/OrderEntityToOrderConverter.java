package com.prodyna.order.conversion;

import static org.springframework.core.convert.TypeDescriptor.collection;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.List;

import org.springframework.stereotype.Component;

import com.prodyna.order.dto.Order;
import com.prodyna.order.dto.OrderItem;
import com.prodyna.order.entity.OrderEntity;
import com.prodyna.order.entity.OrderItemEntity;

@Component
public class OrderEntityToOrderConverter extends AbstractConverter<OrderEntity, Order> {

    @SuppressWarnings("unchecked")
	@Override
    public Order convert(OrderEntity entity) {
    	Order order = new Order();
        order.setOrderId(entity.getId());
        order.setOrderItems((List<OrderItem>) conversionService().convert(entity.getOrderItems(), collection(List.class, valueOf(OrderItemEntity.class)), collection(List.class, valueOf(OrderItem.class))));
        return order;
    }

}
