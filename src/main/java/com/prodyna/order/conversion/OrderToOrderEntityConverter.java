package com.prodyna.order.conversion;

import static org.springframework.core.convert.TypeDescriptor.collection;
import static org.springframework.core.convert.TypeDescriptor.valueOf;

import java.util.List;

import org.springframework.stereotype.Component;

import com.prodyna.order.dto.Order;
import com.prodyna.order.dto.OrderItem;
import com.prodyna.order.entity.OrderEntity;
import com.prodyna.order.entity.OrderItemEntity;

@Component
public class OrderToOrderEntityConverter extends AbstractConverter<Order, OrderEntity> {
    
    @SuppressWarnings("unchecked")
	@Override
    public OrderEntity convert(Order order) {
    	OrderEntity orderEntity = new OrderEntity();
    	orderEntity.setId(order.getOrderId());
    	orderEntity.setOrderItems((List<OrderItemEntity>) conversionService().convert(order.getOrderItems(), collection(List.class, valueOf(OrderItem.class)), collection(List.class, valueOf(OrderItemEntity.class))));
        return orderEntity;
    }

}
