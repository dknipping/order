package com.prodyna.order.conversion;

import org.springframework.stereotype.Component;

import com.prodyna.order.dto.OrderItem;
import com.prodyna.order.entity.OrderItemEntity;

@Component
public class OrderItemEntityToOrderItemConverter extends AbstractConverter<OrderItemEntity, OrderItem> {

	@Override
	public OrderItem convert(OrderItemEntity entity) {
		OrderItem order = new OrderItem();
		order.setItemId(entity.getRefId());
		order.setCount(entity.getCount());
		return order;
	}
	
}


